# Excel Stock Tracker
Google Cloud Function that gets historical stock data and inserts it into an Excel spreadsheet offered for a download.

# Local setup
## Provide API Key
The API key used to talk to the financial API needs to be provided in an environment variable, like this:
```
$ export TIINGO_API_KEY='Your_Tiingo_Key'
```

If you plan to run the function locally, or run any tests, make sure you setup the environment variable before starting Eclipse or running any Gradle commands.

## Eclipse Setup
```text
$ gradle eclipse
```

## Run Unit Tests
```text
$ gradle test
```

## Local Deploy
```text
$ gradle runFunction
$ curl -i localhost:8080
```

### Local Debug
You can connect to port `1044` from Eclipse to set breakpoints in running code.

# Cloud Deploy
```text
$ gradle deploy
```

OR

```text
$ gcloud functions deploy daily-history-function \
    --runtime java11 \
    --trigger-http \
    --memory 512MB \
    --allow-unauthenticated \
    --entry-point vace117.stocktracker.gcp.function.DailyHistoryFunction
```

And check that it works:
```text
$ curl -i <<FUNCTION URL>>

HTTP/2 200 
content-disposition: attachment; filename="Stock_History_2020-12-16.xlsx"
content-type: application/octet-stream
function-execution-id: oycxwdhwt0aq
x-cloud-trace-context: 0bb39085cb7a07ab4c1fb3bdface3e07;o=1
date: Wed, 16 Dec 2020 10:19:52 GMT
server: Google Frontend
content-length: 5350
alt-svc: h3-29=":443"; ma=2592000,h3-T051=":443"; ma=2592000,h3-Q050=":443"; ma=2592000,h3-Q046=":443"; ma=2592000,h3-Q043=":443"; ma=2592000,quic=":443"; ma=2592000; v="46,43"

Warning: Binary output can mess up your terminal. Use "--output -" to tell 
Warning: curl to output it to your terminal anyway, or consider "--output 
Warning: <FILE>" to save to a file.
```

## Provide API Key
Just like for the local code, we need to provide the API key to the function in Google Cloud. Log in to the Google Console, go to your function and manually add your API key as an Environment Variable called `TIINGO_API_KEY`.


# View Logs
```text
$ gcloud functions logs read

D      daily-history-function  oycx6xzlzy0j  2020-12-16 10:21:03.513  Function execution started
I      daily-history-function  oycx6xzlzy0j  2020-12-16 10:21:03.519  Sending query: GET https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=IBM&outputsize=compact&apikey=__CENSORED__ HTTP/1.1
I      daily-history-function  oycx6xzlzy0j  2020-12-16 10:21:04.056  Successfully received 21289 bytes from Alpha Vantage API.
D      daily-history-function  oycx6xzlzy0j  2020-12-16 10:21:05.358  Function execution took 1846 ms, finished with status code: 200

```