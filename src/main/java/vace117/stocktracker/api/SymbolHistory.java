package vace117.stocktracker.api;

import io.vavr.collection.List;

/**
 * The entire Daily History for a Symbol
 *
 * @author Val Blant
 */
public interface SymbolHistory {
    
    public String getSymbol();

    public List<? extends SymbolHistoryDayInfo> getDays();
}
