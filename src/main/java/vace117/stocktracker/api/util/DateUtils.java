package vace117.stocktracker.api.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import io.vavr.control.Option;

public class DateUtils {

    public static final String YYYY_M_D = "yyyy-M-d";
    

    public static Date getDateMinusMonths(Date date, int months) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - months);

        return cal.getTime();
    }    

    public static String formatDate(Date date, String pattern) {
        return Option
                    .of(date)
                    .map( d -> new SimpleDateFormat(pattern).format(d) )
                    .getOrElse("");
    }

}
