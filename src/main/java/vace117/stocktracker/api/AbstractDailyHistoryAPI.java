package vace117.stocktracker.api;

import static io.vavr.API.$;
import static io.vavr.API.Case;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import io.vavr.control.Try;
import vace117.stocktracker.api.APIErrors.HTTPErrorCodeException;
import vace117.stocktracker.api.APIErrors.HTTPRequestFailedException;

/**
 * Common code for talking to financial APIs.
 *
 * @author Val Blant
 */
public abstract class AbstractDailyHistoryAPI {
    protected final Logger logger = Logger.getLogger(getClass().getName());

    
    private EnvironmentVariableReader envVariableReader = new EnvironmentVariableReader();
    
    private HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();


    
    /**
     * @return JSON response from the API for the given symbol
     */
    public Try<SymbolHistory> fetchDailySymbolHistory(String symbol) {
        return _makeHttpRequest( createHttpGetRequestFor(symbol) )
            .onFailure ( ex   -> logger.log(Level.SEVERE, "No JSON could be retrieved from the API!", ex) )
            .flatMap   ( json -> parseSymbolHistory(symbol, json) )
            .onFailure ( ex   -> logger.log(Level.SEVERE, "Unable to parse JSON received from the API!", ex) );            
    }
    
    /**
     * @return Information parsed from JSON into our domain objects// Ugly workaround b/c java generics not smart enough
     */
    protected abstract Try<SymbolHistory> parseSymbolHistory(String symbol, String json);


    /**
     * @return Specific API GET request
     */
    protected abstract HttpGet createHttpGetRequestFor(String symbol);

    /**
     * @return The name of the environment variable that contains the API key for this specific API
     */
    protected abstract String getApiKeyEnvVariableName();
    
    
    

    
    @SuppressWarnings("unchecked")
    private Try<String> _makeHttpRequest(HttpGet request) {
        logger.info("Sending query: " + _sanitize(request.toString()));

        CloseableHttpClient httpClient = _createHttpClient();
        
        return Try.of( () -> httpClient.execute( request ) )
               .flatMap       ( this::_extractResponseBody )
               .onSuccess     ( response -> logger.info("Successfully received " + response.length() + " bytes from API."))
//               .onSuccess     ( response -> logger.info("Response JSON: " + response) )
               .andFinallyTry ( () -> httpClient.close() )
               .mapFailure    ( Case($(), ex -> new HTTPRequestFailedException(request.toString(), ex)) );
    }
    
    private Try<String> _extractResponseBody(CloseableHttpResponse response) {
        final int httpCode = response.getStatusLine().getStatusCode();
        
        return httpCode == HttpStatus.SC_OK ? 
            Try.of     ( () -> EntityUtils.toString(response.getEntity()) ) 
          : Try.failure( new HTTPErrorCodeException(httpCode) );
    }
    
    private CloseableHttpClient _createHttpClient() {
        return httpClientBuilder.build();
    }
    
    protected String getApiKey() {
        return envVariableReader.getApiKey( getApiKeyEnvVariableName() );
    }

    
    private String _sanitize(String url) {
        return url.replaceAll(getApiKey(), "__CENSORED__");
    }

}
