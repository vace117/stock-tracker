package vace117.stocktracker.api;

import io.vavr.control.Option;

/**
 * 
 *
 * @author Val Blant
 */
public class EnvironmentVariableReader {
    String getApiKey(String apiKeyEnvVariableName) {
        return Option
                    .of( System.getenv(apiKeyEnvVariableName) )
                    .getOrElseThrow( 
                        () -> new UnableToAccessApiKeyException(apiKeyEnvVariableName) 
                    );
    }

    @SuppressWarnings("serial")
    public static final class UnableToAccessApiKeyException extends RuntimeException {
        public UnableToAccessApiKeyException(String apiKeyEnvVariableName) {
            super(
                String.format("API Key must be provided in the environment variable called '%s'!", apiKeyEnvVariableName)
            );
        }
    }
    

}
