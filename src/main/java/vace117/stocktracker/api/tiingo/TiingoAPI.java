package vace117.stocktracker.api.tiingo;

import static vace117.stocktracker.api.util.DateUtils.YYYY_M_D;
import static vace117.stocktracker.api.util.DateUtils.formatDate;
import static vace117.stocktracker.api.util.DateUtils.getDateMinusMonths;

import java.util.Date;

import org.apache.http.client.methods.HttpGet;

import io.vavr.Function1;
import io.vavr.control.Try;
import vace117.stocktracker.api.AbstractDailyHistoryAPI;
import vace117.stocktracker.api.SymbolHistory;

/**
 * Talks to the end-point described here:
 * 
 *      https://api.tiingo.com/documentation/end-of-day
 * 
 *
 * @author Val Blant
 */
public class TiingoAPI extends AbstractDailyHistoryAPI {

    @Override
    protected HttpGet createHttpGetRequestFor(String symbol) {
        return new HttpGet(
            "https://api.tiingo.com/tiingo/daily/" + symbol + "/prices"
            + "?startDate=" + _monthsAgo(6)
            + "&columns=date,adjClose,adjVolume"
            + "&token=" + getApiKey()
        );
    }

    @Override
    protected Try<SymbolHistory> parseSymbolHistory(String symbol, String json) {
        return TiingoSymbolHistory
                    .buildFromJSON(symbol, json)
                    .map( Function1.identity() ); // Ugly workaround b/c java generics are not smart enough
    }

    @Override
    protected String getApiKeyEnvVariableName() {
        return "TIINGO_API_KEY";
    }

    private String _monthsAgo(int monthAgo) {
        return formatDate( 
            getDateMinusMonths(new Date(), monthAgo), YYYY_M_D 
        );
    }
    

}
