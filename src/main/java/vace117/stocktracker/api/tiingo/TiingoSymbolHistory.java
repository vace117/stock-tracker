package vace117.stocktracker.api.tiingo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.vavr.collection.List;
import io.vavr.control.Try;
import vace117.stocktracker.api.SymbolHistory;
import vace117.stocktracker.api.SymbolHistoryDayInfo;

/**
 * Represents parsed response from:
 * 
 *      https://api.tiingo.com/documentation/end-of-day
 * 
 * 
 * @author Val Blant
 */
public class TiingoSymbolHistory implements SymbolHistory {
    
    public final String symbol;
    public final List<Day> days;
    
    
    public TiingoSymbolHistory(String symbol, List<Day> days) {
        this.symbol = symbol;
        this.days   = days;
    }

    public final static class Day implements SymbolHistoryDayInfo {
        final public LocalDate  date;
        
        final public BigDecimal close;
        final public Long       volume;
        
        public Day(Map<String, String> dayMap) {
            this.date   = _parseDate       (dayMap.get("date"));
            this.close  = _parseBigDecimal (dayMap.get("adjClose")  );
            this.volume = Long.valueOf     (dayMap.get("adjVolume") );
        }
        
        private BigDecimal _parseBigDecimal(String amount) {
            return new BigDecimal(amount).setScale(2, RoundingMode.DOWN);
        }
        
        private LocalDate _parseDate(String date) {
            return LocalDate.parse( date.replace("T00:00:00.000Z", "") );
        }

        public LocalDate getDate() {
            return date;
        }

        public BigDecimal getClose() {
            return close;
        }

        public Long getVolume() {
            return volume;
        }
    }

    public static Try<TiingoSymbolHistory> buildFromJSON(String symbol, String jsonResponse) {
        return Try.of( () -> {
            List<Day> days = List.ofAll( 
                new ObjectMapper().readValue(
                        jsonResponse, 
                        new TypeReference <java.util.List
                                              < Map<String, String> >
                                          > () {}
                ))
                .map( Day::new )
                .reverse();
            
            return new TiingoSymbolHistory(symbol, days);
        });        
    }

    
    
    public String getSymbol() {
        return symbol;
    }

    public List<Day> getDays() {
        return days;
    }
    

    
}
