package vace117.stocktracker.api;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 *
 * @author Val Blant
 */
@SuppressWarnings("serial")
public class APIErrors {
    private static final Logger logger = Logger.getLogger(APIErrors.class.getName());

    public static final class HTTPErrorCodeException extends RuntimeException {
        public final int httpCode;
        
        private final static String ERROR_MSG_PREFIX = "API returned HTTP code ";

        public HTTPErrorCodeException(int httpCode) {
            super(ERROR_MSG_PREFIX + httpCode);
            this.httpCode = httpCode;
            
            logger.severe(ERROR_MSG_PREFIX + httpCode);
        }
    }
    
    public static final class HTTPRequestFailedException extends RuntimeException {
        public HTTPRequestFailedException(String failedURL, Throwable e) {
            super(_message(failedURL), e);
            
            logger.log(Level.SEVERE, _message(failedURL), e);
        }
        
        private static String _message(String failedURL) {
            return String.format("Call to '%s' has failed!", failedURL);
        }
        
    }
    
}
