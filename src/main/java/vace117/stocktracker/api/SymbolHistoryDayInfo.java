package vace117.stocktracker.api;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Historical information for a single day
 *
 * @author Val Blant
 */
public interface SymbolHistoryDayInfo {
    public LocalDate  getDate();
    
    public BigDecimal getClose();
    public Long       getVolume();

}
