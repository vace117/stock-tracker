package vace117.stocktracker.api.alphavantage;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.vavr.collection.List;
import io.vavr.control.Try;
import vace117.stocktracker.api.SymbolHistory;
import vace117.stocktracker.api.SymbolHistoryDayInfo;

/**
 * Represents parsed response from TIME_SERIES_DAILY api:
 * 
 *      https://www.alphavantage.co/documentation/#daily
 * 
 * 
 * @author Val Blant
 */
@Deprecated // Use TiingoAPI instead!
public class AlphaVantageSymbolHistory implements SymbolHistory {
    
    public String symbol;
    public List<Day> days;
    
    public final static class Day implements SymbolHistoryDayInfo {
        final public LocalDate  date;
        
        final public BigDecimal close;
        final public Long       volume;
        
        public Day(String date, Map<String, String> dateEntry) {
            this.date  = LocalDate.parse(date);
            
            this.close  = _parseBigDecimal( dateEntry.get("4. close")  );
            this.volume = Long.valueOf    ( dateEntry.get("5. volume") );
        }
        
        public Day(Entry<String, Map<String, String>> jsonEntry) {
            this( jsonEntry.getKey(), jsonEntry.getValue() );
        }
        
        private BigDecimal _parseBigDecimal(String amount) {
            return new BigDecimal(amount).setScale(2, RoundingMode.DOWN);
        }

        public LocalDate getDate() {
            return date;
        }

        public BigDecimal getClose() {
            return close;
        }

        public Long getVolume() {
            return volume;
        }
    }

    public static Try<AlphaVantageSymbolHistory> buildFromJSON(String json) {
        return Try.of( 
            () -> new ObjectMapper().readValue(json, AlphaVantageSymbolHistory.class) 
        );        
    }
    
    @JsonProperty("Meta Data")
    private void unpackMetaDataObject(Map<String, String> metaData) {
        symbol = metaData.get("2. Symbol");
    }
    
    @JsonProperty("Time Series (Daily)")
    private void unpackTimeSeriesList(Map<String, Map<String, String>> timeSeries) {
        days = List
                .ofAll( timeSeries.entrySet() )
                .map  ( Day::new );
    }

    public String getSymbol() {
        return symbol;
    }

    public List<Day> getDays() {
        return days;
    }
    

    
}
