package vace117.stocktracker.api.alphavantage;

import org.apache.http.client.methods.HttpGet;

import io.vavr.Function1;
import io.vavr.control.Try;
import vace117.stocktracker.api.AbstractDailyHistoryAPI;
import vace117.stocktracker.api.SymbolHistory;

/**
 * Talks to the end-point described here:
 * 
 *      https://www.alphavantage.co/documentation/#daily
 *          
 * 
 * @author Val Blant
 */
@Deprecated // Use TiingoAPI instead!
public class AlphaVantageAPI extends AbstractDailyHistoryAPI {

    @Override
    protected HttpGet createHttpGetRequestFor(String symbol) {
        return new HttpGet(
                "https://www.alphavantage.co/query"
                + "?function=TIME_SERIES_DAILY"
                + "&symbol=" + symbol 
                + "&outputsize=compact" 
                + "&apikey=" + getApiKey()
        );
    }

    @Override
    protected Try<SymbolHistory> parseSymbolHistory(String symbol, String json) {
        return AlphaVantageSymbolHistory
                    .buildFromJSON(json)
                    .map( Function1.identity() ); // Ugly workaround b/c java generics not smart enough
    }

    @Override
    protected String getApiKeyEnvVariableName() {
        return "ALPHA_VANTAGE_API_KEY";
    }      
    
}
