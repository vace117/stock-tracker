package vace117.stocktracker.gcp.function;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;
import org.apache.http.entity.ContentType;

import com.google.cloud.functions.HttpFunction;
import com.google.cloud.functions.HttpRequest;
import com.google.cloud.functions.HttpResponse;

import io.vavr.collection.List;
import io.vavr.control.Try;
import vace117.stocktracker.api.AbstractDailyHistoryAPI;
import vace117.stocktracker.api.SymbolHistory;
import vace117.stocktracker.api.tiingo.TiingoAPI;
import vace117.stocktracker.excel.ExcelWriter;

/**
 * 
 * @author Val Blant
 */
public class DailyHistoryFunction implements HttpFunction {
    private static final Logger logger = Logger.getLogger(DailyHistoryFunction.class.getName());
    
    private AbstractDailyHistoryAPI stocksAPI = new TiingoAPI();
    
    private final static List<String> SYMBOLS = List.of(
        "TD", 
        "QQQ", 
        "VV", 
        "VT", 
        "ARKG", 
        "VONG", 
        "DIA"
    ); 

    /**
     * This is the main entry point called by Google Cloud runtime when you make a request
     * to the trigger URL. 
     */
    @Override
    public void service(HttpRequest request, HttpResponse response) throws Exception {
        _generateExcelFile( 
            SYMBOLS.map( stocksAPI::fetchDailySymbolHistory ) 
        )
        
        .andThenTry( inputStream -> _sendExcelFileInResponse(inputStream, response) )
        .onFailure ( ex          -> _sendErrorResponse      (ex,          response) );
        
    }
    
    
    
    
    private Try<ByteArrayInputStream> _generateExcelFile(List<Try<? extends SymbolHistory>> maybeStockInfos) {
        return _validatedStockInfos(maybeStockInfos)
            .flatMap( stockInfos -> new ExcelWriter(stockInfos).generateExcelFile() )
            .onFailure( ex -> logger.log(Level.SEVERE, "Unable to generate Excel file!", ex) );
    }
    
    private Try<List<? extends SymbolHistory>> _validatedStockInfos(List<Try<? extends SymbolHistory>> stockInfos) {
        List<Try<? extends SymbolHistory>> failedSymbols = stockInfos.filter(si -> si.isFailure());

        return failedSymbols.isEmpty() ? 
            Try.success( stockInfos.map( si -> si.get() )) 
          : Try.failure( new RuntimeException("Unable to retrieve information for the following symbols: " +  failedSymbols) );
    }
    
    private void _sendExcelFileInResponse(ByteArrayInputStream in, HttpResponse response) throws Exception {
        response.setContentType(ContentType.APPLICATION_OCTET_STREAM.getMimeType());
        response.appendHeader("Content-Disposition", "attachment; filename=\"Stock_History_" + LocalDate.now() + ".xlsx\"");
        
        IOUtils.copy(in, response.getOutputStream());
    }
    
    private void _sendErrorResponse(Throwable ex, HttpResponse response) {
        logger.log(Level.SEVERE, "Writing error response, b/c an error has occured.", ex);
        
        response.setStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        
        Try
            .run( () -> IOUtils.write(ex.getMessage(), response.getOutputStream(), Charset.defaultCharset()) )
            .onFailure( anotherEx -> {
                logger.log(Level.SEVERE, "Unable to write error response!", anotherEx);
                throw new RuntimeException(anotherEx);
            });
    }
}
