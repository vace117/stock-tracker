package vace117.stocktracker.excel;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import io.vavr.Function0;
import io.vavr.collection.List;
import io.vavr.control.Option;
import io.vavr.control.Try;
import vace117.stocktracker.api.SymbolHistory;

/**
 * Renders <code>TimeSeriesDailyResponse</code>s into an Excel file.
 * 
 * @author Val Blant
 */
public class ExcelWriter {
    
    private final List<? extends SymbolHistory> timeSeries;
    
    private ExcelStyles styles;
    private XSSFSheet sheet;


    public ExcelWriter(List<? extends SymbolHistory> timeSeries) {
        this.timeSeries = timeSeries;
    }
    
    

    public Try<ByteArrayInputStream> generateExcelFile()  {
        return Try
                .withResources( () -> _createWorkbook() )
                .of( this::_populateWorkbook );
    }
    
    
    /**
     * The content of the sheet is defined here
     */
    private XSSFSheet _populateSheetData(XSSFSheet sheet) {
        _writeBlankRow();
        
        _writeHeaderRow();

        _writeDataRows();
        
        return sheet;
    }
    

    
    
    
    
    
    private XSSFWorkbook _createWorkbook() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        
        styles = new ExcelStyles(workbook);
        
        return workbook;
    }
    
    private ByteArrayInputStream _populateWorkbook(XSSFWorkbook workbook) throws IOException {
        return 
            _saveWorkbook(
                _populateSheetData(
                    _createSheet(workbook)
                )
            );
    }
    
    private XSSFSheet _createSheet(XSSFWorkbook workbook) {
        this.sheet = workbook.createSheet("Daily Closing Amounts");
        
        return sheet;
    }
    
    
    private void _writeBlankRow() {
        sheet.createRow(0);
    }
    
    private void _writeHeaderRow() {
        final Row headerRow1 = sheet.createRow(1);
        final Row headerRow2 = sheet.createRow(2);
        
        timeSeries
            .map( stockInfo -> stockInfo.getSymbol() )
            .forEachWithIndex( (symbol, stockIndex)  -> {
                _textCell  (headerRow1, _symbolStartColumn(stockIndex) + 0, symbol, styles.BOLD_HEADER);
                _emptyCell (headerRow1, _symbolStartColumn(stockIndex) + 1);
                _emptyCell (headerRow1, _symbolStartColumn(stockIndex) + 2);
                
                _textCell  (headerRow2, _symbolStartColumn(stockIndex) + 0, "Date",  styles.NORMAL_CENTERED_HEADER);
                _textCell  (headerRow2, _symbolStartColumn(stockIndex) + 1, "Close", styles.NORMAL_CENTERED_HEADER);
                _emptyCell (headerRow2, _symbolStartColumn(stockIndex) + 2);
            });
    }
    
    private void _writeDataRows() {
        final int START_ROW = 3;
        
        timeSeries
            .map( stockInfo -> stockInfo.getDays() )
            .forEachWithIndex( (days, stockIndex)  -> {
                days.forEachWithIndex( (day, dayIndex) -> {
                    Row row = Option
                                .of       (       sheet.getRow   (START_ROW + dayIndex) )
                                .getOrElse( () -> sheet.createRow(START_ROW + dayIndex) );

                    _dateCell  (row, _symbolStartColumn(stockIndex) + 0, day.getDate());
                    _moneyCell (row, _symbolStartColumn(stockIndex) + 1, day.getClose());
                    _emptyCell (row, _symbolStartColumn(stockIndex) + 2);
                });
                
                sheet.autoSizeColumn( _symbolStartColumn(stockIndex) + 0 );
                sheet.autoSizeColumn( _symbolStartColumn(stockIndex) + 1 );
                sheet.setColumnWidth( _symbolStartColumn(stockIndex) + 2, 256 * 3 );
            });
    }
    
    
    private int _symbolStartColumn(int symbolIndex) {
        return symbolIndex * 3;
    }
    
    private ByteArrayInputStream _saveWorkbook(XSSFSheet sheet) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        sheet.getWorkbook().write(baos);
        baos.close();

        this.styles = null;
        this.sheet = null;

        return new ByteArrayInputStream(baos.toByteArray());
    }

    
    
    
    private Cell _emptyCell(Row row, int columnIndex) {
        return _textCell(row, columnIndex, "");
    }
    private Cell _textCell(Row row, int columnIndex, String data) {
        return _textCell(row, columnIndex, data, styles.DEFAULT_CELL);
    }
    private Cell _textCell(Row row, int columnIndex, String data, Function0<CellStyle> style) {
        Cell cell = row.createCell(columnIndex);
        cell.setCellValue(data);
        cell.setCellStyle(style.get());
        
        return cell;
    }

    private Cell _dateCell(Row row, int columnIndex, LocalDate data) {
        Cell cell = row.createCell(columnIndex);
        cell.setCellValue(data);
        cell.setCellStyle(styles.DATE_CELL.get());
        
        return cell;
    }

    private Cell _moneyCell(Row row, int columnIndex, BigDecimal data) {
        Cell cell = row.createCell(columnIndex);
        cell.setCellValue(data.doubleValue());
        cell.setCellStyle(styles.MONEY_CELL.get());
        
        return cell;
    }

}
