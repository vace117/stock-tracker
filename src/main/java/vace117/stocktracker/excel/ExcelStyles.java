package vace117.stocktracker.excel;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import io.vavr.Function0;

/**
 * Defines Excel cell styles we use. The styles are created once and then cached.
 * 
 * @author Val Blant
 */
public class ExcelStyles {
    private XSSFWorkbook workbook;

    public ExcelStyles(XSSFWorkbook workbook) {
        this.workbook = workbook;
    }
    
    final Function0<Font> DEFAULT_FONT = Function0.of( () -> {
        Font font = workbook.createFont();
        _setFontDefaults(font);
        
        return font;
    }).memoized();
    
    final Function0<CellStyle> DEFAULT_CELL = Function0.of( () -> {
        CellStyle style = workbook.createCellStyle();
        style.setFont(DEFAULT_FONT.get());
        
        return style;
    }).memoized();
    
    final Function0<Font> BOLD_FONT = Function0.of( () -> {
        Font font = workbook.createFont();
        font.setBold(true);
        _setFontDefaults(font);
        font.setFontHeightInPoints((short)14);        
        
        return font;
    }).memoized();
    
    final Function0<CellStyle> BOLD_HEADER = Function0.of( () -> {
        CellStyle style = workbook.createCellStyle();
        style.setFont(BOLD_FONT.get());
        style.setWrapText(true);
        
        return style;
    }).memoized();

    final Function0<CellStyle> NORMAL_CENTERED_HEADER = Function0.of( () -> {
        CellStyle style = workbook.createCellStyle();
        style.setFont(DEFAULT_FONT.get());
        style.setAlignment(HorizontalAlignment.CENTER);
        
        return style;
    }).memoized();

    void _setFontDefaults(Font font) {
        font.setFontHeightInPoints((short)12);
        font.setFontName("Arial");
    }
    
    
    final Function0<CellStyle> DATE_CELL = Function0.of( () -> {
        CellStyle style = workbook.createCellStyle();
        CreationHelper createHelper = workbook.getCreationHelper();
        style.setDataFormat(createHelper.createDataFormat().getFormat("mm/dd/yyyy"));
        style.setFont(DEFAULT_FONT.get());
        style.setAlignment(HorizontalAlignment.CENTER);
        
        return style;
    }).memoized();

    final Function0<CellStyle> MONEY_CELL = Function0.of( () -> {
        CellStyle style = workbook.createCellStyle();
        style.setDataFormat((short)7); // "$#,##0.00);($#,##0.00)"
        style.setFont(DEFAULT_FONT.get());
        style.setAlignment(HorizontalAlignment.RIGHT);
        
        return style;
    }).memoized();
    
    
}
