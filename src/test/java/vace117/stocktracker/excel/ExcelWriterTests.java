package vace117.stocktracker.excel;

import static org.junit.Assert.assertTrue;
import static vace117.stocktracker.util.TestUtils.readFileFromClasspath;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.vavr.collection.List;
import vace117.stocktracker.api.tiingo.TiingoSymbolHistory;

/**
 * 
 * @author Val Blant
 */
@RunWith(EasyMockRunner.class)
public class ExcelWriterTests extends EasyMockSupport {
    
    ExcelWriter excelWriter;
    
    @Before
    public void prepareTestData() {
        excelWriter = new ExcelWriter(
            List.of(
                _parseFile("IBM",  "tiingo_daily_symbol_history_ibm.json"),
                _parseFile("PYPL", "tiingo_daily_symbol_history_pypl.json")
            )
        );
    }
    
    @Test
    public void shouldGenerateAnExcelFile() throws Exception {
        byte[] excelFile = IOUtils.toByteArray(excelWriter.generateExcelFile().get());
        
        assertTrue("Exepcted non-empty Excel file!", excelFile.length > 3200);
        
        FileUtils.writeByteArrayToFile(new File("/tmp/symbol_history_test.xlsx"), excelFile);        
    }
    
    private TiingoSymbolHistory _parseFile(String symbol, String fileName) {
        return TiingoSymbolHistory.buildFromJSON( symbol, readFileFromClasspath(fileName) ).get();
    }

}
