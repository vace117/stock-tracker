package vace117.stocktracker.gcp.function;

import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertTrue;
import static vace117.stocktracker.util.TestUtils.readFileFromClasspath;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpStatus;
import org.apache.http.entity.ContentType;
import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.cloud.functions.HttpFunction;
import com.google.cloud.functions.HttpRequest;
import com.google.cloud.functions.HttpResponse;

import io.vavr.Function1;
import io.vavr.control.Try;
import vace117.stocktracker.api.APIErrors.HTTPRequestFailedException;
import vace117.stocktracker.api.AbstractDailyHistoryAPI;
import vace117.stocktracker.api.alphavantage.AlphaVantageSymbolHistory;

/**
 * 
 * @author Val Blant
 */
@RunWith(EasyMockRunner.class)
public class DailyHistoryFunctionTests extends EasyMockSupport {
    @Mock HttpRequest request;
    @Mock HttpResponse response;
    @Mock AbstractDailyHistoryAPI stockAPI;
    
    @TestSubject DailyHistoryFunction stockHistory = new DailyHistoryFunction();

    private ByteArrayOutputStream baos;
    
    @Before
    public void setup() throws IOException {
        baos = new ByteArrayOutputStream();

        // Mock the OutputStream and capture all output into our variable
        //
        expect(response.getOutputStream())
            .andReturn(baos)
            .anyTimes();
    }

    @Test
    public void shouldBeAGoogleCloudFunction() {
        assertTrue("Expected an HttpFunction!", stockHistory instanceof HttpFunction );
    }

    @Test
    public void shouldReceiveNonEmptyResponse() throws Exception {
        expect(stockAPI.fetchDailySymbolHistory(anyString()))
            .andReturn( 
                AlphaVantageSymbolHistory
                    .buildFromJSON(readFileFromClasspath("alphavantage_daily_time_series_ibm.json"))
                    .map( Function1.identity() ) // Ugly workaround b/c java generics not smart enough
            )
            .anyTimes();
        
        response.setContentType(ContentType.APPLICATION_OCTET_STREAM.getMimeType());
        expectLastCall();
        
        response.appendHeader(anyString(), anyString());
        expectLastCall();
        
        replayAll();
        stockHistory.service(request, response);
        verifyAll();
        
        assertTrue("Expected non-empty response", baos.toByteArray().length > 1000);
    }

    @Test
    public void shouldReturn500InCaseOfJSONParsingError() throws Exception {
        expect(stockAPI.fetchDailySymbolHistory(anyString()))
            .andReturn( Try.failure( new RuntimeException("Parsing error") ) )
            .anyTimes();

        response.setStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        expectLastCall();

        replayAll();
        stockHistory.service(request, response);
        verifyAll();
        
        assertTrue(_functionResponse().contains("Parsing error"));
    }
    
    @Test
    public void shouldReturn500InCaseOfHTTPError() throws Exception {
        final HTTPRequestFailedException EXPECTED_EXCEPTION = new HTTPRequestFailedException("TEST_URL", new RuntimeException("Don't think so!"));
        
        expect(stockAPI.fetchDailySymbolHistory(anyString()))
            .andReturn( Try.failure(EXPECTED_EXCEPTION) )
            .anyTimes();
        
        response.setStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        expectLastCall();

        replayAll();
        stockHistory.service(request, response);
        verifyAll();
        
        assertTrue(_functionResponse().contains(EXPECTED_EXCEPTION.getMessage()));
    }
    
    
    
    
    private String _functionResponse() throws IOException {
        return baos.toString();
    }

}
