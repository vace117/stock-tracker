package vace117.stocktracker.util;

import java.nio.file.Files;
import java.nio.file.Paths;

import io.vavr.control.Try;

public class TestUtils {

    public static String readFileFromClasspath(final String fileName) {
        return Try.of( () -> 
            new String(
                Files.readAllBytes(
                    Paths.get(
                        TestUtils.class
                            .getClassLoader()
                            .getResource(fileName)
                            .toURI()
                    )
                )
            )            
        ).getOrElseThrow( ex -> new RuntimeException(ex) );
    }

    
}
