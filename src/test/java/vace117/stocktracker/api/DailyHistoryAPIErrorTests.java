package vace117.stocktracker.api;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.vavr.control.Try;
import vace117.stocktracker.api.APIErrors.HTTPErrorCodeException;
import vace117.stocktracker.api.APIErrors.HTTPRequestFailedException;
import vace117.stocktracker.api.tiingo.TiingoAPI;

@RunWith(EasyMockRunner.class)
public class DailyHistoryAPIErrorTests extends EasyMockSupport {
    
    @Mock HttpClientBuilder httpClientBuilder;
    @Mock CloseableHttpClient httpClient;
    @Mock CloseableHttpResponse response;
    @Mock StatusLine statusLine;
    @Mock EnvironmentVariableReader envVariableReader;
    
    @TestSubject AbstractDailyHistoryAPI stockAPI = new TiingoAPI();
    
    @Before
    public void setup() {
        expect( httpClientBuilder.build() ).andReturn(httpClient).once();
        expect( envVariableReader.getApiKey(anyString()) ).andReturn( "api_key" ).anyTimes();
    }
    
    @Test
    public void testHTTPRequestFailedException() throws Exception {
        expect( httpClient.execute(anyObject()) )
            .andThrow( new IOException("Don't think so!"))
            .once();
        
        _expectConnectionClose();
        
        replayAll();

        
        Try<? extends SymbolHistory> result = stockAPI.fetchDailySymbolHistory("IBM");
        assertEquals(true, result.isFailure());
        assertEquals(HTTPRequestFailedException.class, result.getCause().getClass());
    }

    @Test
    public void testHTTPErrorCodeException() throws Exception {
        expect( httpClient.execute(anyObject()) ).andReturn(response)  .once();
        expect( response.getStatusLine() )       .andReturn(statusLine).once();
        expect( statusLine.getStatusCode() )     .andReturn(500)       .once();
        _expectConnectionClose();
        
        replayAll();

        
        Try<? extends SymbolHistory> result = stockAPI.fetchDailySymbolHistory("IBM");
        assertEquals(true, result.isFailure());
        assertEquals(HTTPRequestFailedException.class, result.getCause().getClass());
        assertEquals(HTTPErrorCodeException.class,     result.getCause().getCause().getClass());
    }

    private void _expectConnectionClose() throws Exception {
        httpClient.close();
        expectLastCall();   
    }
}
