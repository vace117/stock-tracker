package vace117.stocktracker.api.alphavantage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;

import vace117.stocktracker.api.SymbolHistory;

/**
 * 
 *
 * @author Val Blant
 */
@Deprecated // Use TiingoAPI instead!
@Ignore
public class AlphaVantageAPIIntegrationTests {
    AlphaVantageAPI api = new AlphaVantageAPI();
    
    @Test
    public void shouldReceiveSymbolHistory() throws Exception {
        
        SymbolHistory history = api.fetchDailySymbolHistory("IBM").get();
        
        assertNotNull("Expected a history!", history);

        assertEquals("IBM", history.getSymbol());
        assertTrue("Expected a history!", history.getDays().size() > 5);
    }

}
