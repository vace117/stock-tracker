package vace117.stocktracker.api.alphavantage;

import static org.junit.Assert.assertEquals;
import static vace117.stocktracker.util.TestUtils.readFileFromClasspath;

import org.junit.Test;

import vace117.stocktracker.api.alphavantage.AlphaVantageSymbolHistory.Day;

/**
 * Make sure that we can parse JSON responses from AlphaVantage TIME_SERIES_DAILY api.
 * 
 * @author Val Blant
 */
@Deprecated // Use TiingoAPI instead!
public class AlphaVantageSymbolHistoryTests {
    
    @Test
    public void shouldDeserializeJSONResponse() throws Exception {
        final AlphaVantageSymbolHistory response = 
                AlphaVantageSymbolHistory
                    .buildFromJSON( readFileFromClasspath("alphavantage_daily_time_series_ibm.json") )
                    .get();

        assertEquals("IBM", response.symbol);
        assertEquals(100,   response.days.size());        
        
        final Day mostRecentDay = response.days.head();
        assertEquals("2020-12-14", mostRecentDay.date.toString());
        assertEquals("123.53",     mostRecentDay.close.toString());
        assertEquals("4553716",    mostRecentDay.volume.toString());
    }

}
