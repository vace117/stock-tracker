package vace117.stocktracker.api.tiingo;

import static org.junit.Assert.assertEquals;
import static vace117.stocktracker.util.TestUtils.readFileFromClasspath;

import org.junit.Test;

import vace117.stocktracker.api.tiingo.TiingoSymbolHistory.Day;


/**
 * Make sure that we can parse JSON responses from Tiingo API:
 * 
 *      https://api.tiingo.com/documentation/end-of-day
 *      
 * 
 * @author Val Blant
 */
public class TiingoSymbolHistoryTests {
    
    @Test
    public void shouldDeserializeJSONResponse() throws Exception {
        final TiingoSymbolHistory response = 
                TiingoSymbolHistory
                    .buildFromJSON( "IBM", readFileFromClasspath("tiingo_daily_symbol_history_ibm.json") )
                    .get();

        assertEquals("IBM", response.symbol);
        assertEquals(246,   response.days.size());        
        
        final Day mostRecentDay = response.days.head();
        assertEquals("2020-12-21", mostRecentDay.date.toString());
        assertEquals("237.72",     mostRecentDay.close.toString());
        assertEquals("8156045",    mostRecentDay.volume.toString());
    }

}
