package vace117.stocktracker.api.tiingo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import vace117.stocktracker.api.SymbolHistory;

/**
 * 
 *
 * @author Val Blant
 */
public class TiingoAPIIntegrationTests {
    TiingoAPI api = new TiingoAPI();
    
    @Test
    public void shouldReceiveSymbolHistory() throws Exception {
        
        SymbolHistory history = api.fetchDailySymbolHistory("IBM").get();
        
        assertNotNull("Expected a history!", history);

        assertEquals("IBM", history.getSymbol());
        assertTrue("Expected a history!", history.getDays().size() > 5);
    }

}
