package vace117.stocktracker.api.util;

import static org.junit.Assert.assertEquals;
import static vace117.stocktracker.api.util.DateUtils.YYYY_M_D;
import static vace117.stocktracker.api.util.DateUtils.formatDate;
import static vace117.stocktracker.api.util.DateUtils.getDateMinusMonths;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import io.vavr.control.Try;

/**
 * 
 *
 * @author Val Blant
 */
public class DateUtilsTests {
    
    private final DateFormat DATE_FORMATTER = new SimpleDateFormat(YYYY_M_D);

    @Test
    public void testGetDateMinusMonths() {
        Date yearAgo = getDateMinusMonths( _createDate("2020-12-22"), 13 );
        
        assertEquals("2019-11-22", DATE_FORMATTER.format(yearAgo));
    }
    
    @Test
    public void testFormatDate() {
        assertEquals("",         formatDate(null, YYYY_M_D));
        assertEquals("2020-1-1", formatDate(_createDate("2020-1-1"), YYYY_M_D));
    }
    
    private Date _createDate(String dateString) {
        return Try.of( () -> DATE_FORMATTER.parse(dateString) ).get();
    }
}
